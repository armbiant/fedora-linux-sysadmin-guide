
:experimental:
include::{partialsdir}/entities.adoc[]

= Preface

The [citetitle]_System Administrator's Guide_ contains information on how to customize the {MAJOROSVER} system to fit your needs. If you are looking for a comprehensive, task-oriented guide for configuring and customizing your system, this is the manual for you.

This manual discusses many intermediate topics such as the following:

* Installing and managing packages using [application]*DNF*

* Configuring [application]*Apache HTTP Server*, [application]*Postfix*, [application]*Sendmail* and other enterprise-class servers and software

* Working with kernel modules and upgrading the kernel

[NOTE]
====

Some of the graphical procedures and menu locations are specific to GNOME, but most command line instructions will be universally applicable.

====

[[sect-Preface-Target_Audience]]
== Target Audience

The [citetitle]_System Administrator's Guide_ assumes you have a basic understanding of the {MAJOROS} operating system. If you need help with the installation of this system, refer to the link:++https://docs.fedoraproject.org/install-guide++[{MAJOROS} Installation Guide].

[[sect-Preface-Book_Organization]]
== How to Read this Book

This manual is divided into the following main categories:

xref:basic-system-configuration/intro-basic-system-configuration.adoc[Basic System Configuration]::  This part covers basic system administration tasks such as keyboard configuration, date and time configuration, managing users and groups, and gaining privileges.
+
xref:basic-system-configuration/Opening_GUI_Applications.adoc[Opening Graphical Applications] describes methods for opening `Graphical User Interface`, or _GUI_, applications in various environments.
+
xref:basic-system-configuration/System_Locale_and_Keyboard_Configuration.adoc[System Locale and Keyboard Configuration] covers basic language and keyboard setup. Read this chapter if you need to configure the language of your desktop, change the keyboard layout, or add the keyboard layout indicator to the panel.
+
xref:basic-system-configuration/Configuring_the_Date_and_Time.adoc[Configuring the Date and Time] covers the configuration of the system date and time. Read this chapter if you need to set or change the date and time.
+
xref:basic-system-configuration/Managing_Users_and_Groups.adoc[Managing Users and Groups] covers the management of users and groups in a graphical user interface and on the command line. Read this chapter if you need to manage users and groups on your system, or enable password aging.
+
xref:basic-system-configuration/Gaining_Privileges.adoc[Gaining Privileges] covers ways to gain administrative privileges using setuid programs such as [command]#su# and [command]#sudo#.

xref:package-management/intro-package-management.adoc[Package Management]::  This part describes how to manage software packages on {MAJOROS} using [application]*DNF*.
+
xref:package-management/DNF.adoc[DNF] describes the [application]*DNF* package manager. Read this chapter for information how to search, install, update, and uninstall packages on the command line.

xref:infrastructure-services/intro-infrastructure-services.adoc[Infrastructure Services]::  This part provides information on how to configure services and daemons, configure authentication, and enable remote logins.
+
xref:infrastructure-services/Services_and_Daemons.adoc[Services and Daemons] covers the configuration of the services to be run when a system is started, and provides information on how to start, stop, and restart the services on the command line using the [command]#systemctl# utility.
+
xref:infrastructure-services/OpenSSH.adoc[OpenSSH] describes how to enable a remote login via the SSH protocol. It covers the configuration of the `sshd` service, as well as a basic usage of the [command]#ssh#, [command]#scp#, [command]#sftp# client utilities. Read this chapter if you need a remote access to a machine.
+
xref:infrastructure-services/TigerVNC.adoc[TigerVNC] describes the _virtual network computing_ (*VNC*) method of graphical desktop sharing which allows you to remotely control other computers.

xref:servers/intro-servers.adoc[Servers]::  This part discusses various topics related to servers such as how to set up a Web server or share files and directories over the network.
+
xref:servers/Web_Servers.adoc[Web Servers] focuses on the [application]*Apache HTTP Server*, a robust, full-featured open source web server developed by the Apache Software Foundation. Read this chapter if you need to configure a web server on your system.
+
xref:servers/Mail_Servers.adoc[Mail Servers] reviews modern email protocols in use today, and some of the programs designed to send and receive email, including [application]*Postfix*, [application]*Sendmail*, [application]*Fetchmail*, and [application]*Procmail*. Read this chapter if you need to configure a mail server on your system.
+
xref:servers/Directory_Servers.adoc[Directory Servers] covers the installation and configuration of [application]*OpenLDAP*, an open source implementation of the LDAPv2 and LDAPv3 protocols. Read this chapter if you need to configure a directory server on your system.
+
xref:servers/File_and_Print_Servers.adoc[File and Print Servers] guides you through the installation and configuration of [application]*Samba*, an open source implementation of the Server Message Block (SMB) protocol, and [application]*vsftpd*, the primary FTP server shipped with {MAJOROS}. Additionally, it explains how to use the [application]*Printer Configuration* tool to configure printers. Read this chapter if you need to configure a file or print server on your system.
+
xref:servers/Configuring_NTP_Using_the_chrony_Suite.adoc[Configuring NTP Using the chrony Suite] covers the installation and configuration of the [application]*chrony* suite, a client and a server for the Network Time Protocol (`NTP`). Read this chapter if you need to configure the system to synchronize the clock with a remote `NTP` server, or set up an `NTP` server on this system.
+
xref:servers/Configuring_NTP_Using_ntpd.adoc[Configuring NTP Using ntpd] covers the installation and configuration of the `NTP` daemon, `ntpd`, for the Network Time Protocol (`NTP`). Read this chapter if you need to configure the system to synchronize the clock with a remote `NTP` server, or set up an `NTP` server on this system, and you prefer not to use the [application]*chrony* application.
+
xref:servers/Configuring_PTP_Using_ptp4l.adoc[Configuring PTP Using ptp4l] covers the installation and configuration of the Precision Time Protocol application, [application]*ptp4l*, an application for use with network drivers that support the Precision Network Time Protocol (`PTP`). Read this chapter if you need to configure the system to synchronize the system clock with a master `PTP` clock.

xref:monitoring-and-automation/intro-monitoring-and-automation.adoc[Monitoring and Automation]::  This part describes various tools that allow system administrators to monitor system performance, automate system tasks, and report bugs.
+
xref:monitoring-and-automation/System_Monitoring_Tools.adoc[System Monitoring Tools] discusses applications and commands that can be used to retrieve important information about the system. Read this chapter to learn how to gather essential system information.
+
xref:monitoring-and-automation/Viewing_and_Managing_Log_Files.adoc[Viewing and Managing Log Files] describes the configuration of the `rsyslog` daemon, and explains how to locate, view, and monitor log files. Read this chapter to learn how to work with log files.
+
xref:monitoring-and-automation/Automating_System_Tasks.adoc[Automating System Tasks] provides an overview of the [command]#cron#, [command]#at#, and [command]#batch# utilities. Read this chapter to learn how to use these utilities to perform automated tasks.
+
xref:monitoring-and-automation/OProfile.adoc[OProfile] covers [application]*OProfile*, a low overhead, system-wide performance monitoring tool. Read this chapter for information on how to use [application]*OProfile* on your system.

xref:kernel-module-driver-configuration/intro-kernel-module-driver-configuration.adoc[Kernel, Module and Driver Configuration]::  This part covers various tools that assist administrators with kernel customization.
+
xref:kernel-module-driver-configuration/Working_with_the_GRUB_2_Boot_Loader.adoc[Working with the GRUB 2 Boot Loader] describes the GNU GRand Unified Boot loader (GRUB) version 2 boot loader, which enables selecting an operating system or kernel to be loaded at system boot time.
+
xref:kernel-module-driver-configuration/Manually_Upgrading_the_Kernel.adoc[Manually Upgrading the Kernel] provides important information on how to manually update a kernel package using the [command]#rpm# command instead of [command]#dnf#. Read this chapter if you cannot update a kernel package with the [application]*DNF* package manager.
+
xref:kernel-module-driver-configuration/Working_with_Kernel_Modules.adoc[Working with Kernel Modules] explains how to display, query, load, and unload kernel modules and their dependencies, and how to set module parameters. Additionally, it covers specific kernel module capabilities such as using multiple Ethernet cards and using channel bonding. Read this chapter if you need to work with kernel modules.

xref:RPM.adoc[RPM]::  This appendix concentrates on the RPM Package Manager (RPM), an open packaging system used by {MAJOROS}, and the use of the [command]#rpm# utility. Read this appendix if you need to use [command]#rpm# instead of [command]#dnf#.

xref:Wayland.adoc[The Wayland Display Server]:: This appendix looks at Wayland, a new display server used in GNOME for {MAJOROS} and how to troubleshoot issues with the Wayland display server.

include::{partialsdir}/Feedback.adoc[]

[[pref-Acknowledgments]]
== Acknowledgments

Certain portions of this text first appeared in the [citetitle]_Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 System Administrator's Guide_, copyright &copy; 2014&ndash;{YEAR} Red{nbsp}Hat, Inc., available at link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/index.html++[].

xref:monitoring-and-automation/System_Monitoring_Tools.adoc#sect-System_Monitoring_Tools-Net-SNMP[Monitoring Performance with Net-SNMP] is based on an article written by Michael Solberg.

The authors of this book would like to thank the following people for their valuable contributions: Adam Tkáč, Andrew Fitzsimon, Andrius Benokraitis, Brian Cleary Edward Bailey, Garrett LeSage, Jeffrey Fearn, Joe Orton, Joshua Wulf, Karsten Wade, Lucy Ringland, Marcela Mašláňová, Mark Johnson, Michael Behm, Miroslav Lichvár, Radek Vokál, Rahul Kavalapara, Rahul Sundaram, Sandra Moore, Zbyšek Mráz, Jan Včelák, Peter Hutterer, T.C. Hollingsworth, and James Antill, among many others.
