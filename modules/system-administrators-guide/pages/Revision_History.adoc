
:experimental:

[[app-Revision_History]]
= Revision History

Note that revision numbers relate to the edition of this manual, not to version numbers of Fedora.

`1-10`:: Tue Oct 30 2018, Petr Bokoč (pbokoc@redhat.com)

* Fedora 29 Final release

* All external links are now converted to HTTPS where applicable (link:++https://pagure.io/fedora-docs/system-administrators-guide/issue/7++[issue 7])

* The chapter about *anacron* now mentions that it won't run by default when the system is being powered by a battery (link:++https://pagure.io/fedora-docs/system-administrators-guide/issue/8++[issue 8])

* Fixed a keyring name in the Kernel Module Authentication chapter (link:++https://pagure.io/fedora-docs/system-administrators-guide/issue/11++[issue 11])

* Removed mentions of *rsyslog* as a default syslog

* Various fixes related to the new publishing system

`1-9`:: Sun Jun 25, 2017, Ryan       (t3rm1n4l@fedoraproject.org)

* Converted document to asciidocs, updated table refs, removed manual line breaks, fixed formatting and some grammar

`1-8`:: Mon Nov 14 2016, Petr Bokoč (pbokoc@redhat.com)

* Fedora 25 release of the [citetitle]_System Administrator's Guide_.

`1-7`:: Tue June 21 2016, Stephen Wadeley (swadeley@redhat.com)

* Fedora 24 release of the [citetitle]_System Administrator's Guide_.

`1-5`:: Mon Nov 02 2015, Stephen Wadeley (swadeley@redhat.com)

* Fedora 23 release of the [citetitle]_System Administrator's Guide_.

`1-4.1`:: Tue Oct 27 2015, Stephen Wadeley (swadeley@redhat.com)

* Added "`Gaining Privileges`" chapter, "`Using OpenSSH Certificate Authentication`" section, and made improvements to the GRUB 2 chapter.

`1-4`:: Mon May 25 2015, Stephen Wadeley (swadeley@redhat.com)

* Fedora 22 release of the [citetitle]_System Administrator's Guide_.

`1-3`:: Mon Apr 4 2015, Stephen Wadeley (swadeley@redhat.com)

* Replaced Yum chapter with DNF chapter.

`1-2.1`:: Wed Mar 4 2015, Stephen Wadeley (swadeley@redhat.com)

* Added "`Working with the GRUB 2 Boot Loader`" chapter.

`1-2`:: Tue Dec 9 2014, Stephen Wadeley (swadeley@redhat.com)

* Fedora 21 release of the [citetitle]_System Administrator's Guide_.

`1-1`:: Thu Aug 9 2012, Jaromír Hradílek (jhradilek@redhat.com)

* Updated Network Interfaces.

`1-0`:: Tue May 29 2012, Jaromír Hradílek (jhradilek@redhat.com)

* Fedora 17 release of the [citetitle]_System Administrator's Guide_.
